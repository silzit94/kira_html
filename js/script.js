function uploadFile(target, num) {
  const targetId = document.getElementById("file-name-" + num);

  targetId.innerHTML = target.files[0].name;
  targetId.classList.remove('placeholder-file')
}

document.addEventListener("DOMContentLoaded", function () {
  // make it as accordion for smaller screens
  if (window.innerWidth > 992) {
    document
      .querySelectorAll(".navbar .nav-item")
      .forEach(function (everyitem) {
        everyitem.addEventListener("mouseover", function (e) {
          let el_link = this.querySelector("a[data-bs-toggle]");

          if (el_link != null) {
            let nextEl = el_link.nextElementSibling;
            el_link.classList.add("show");
            nextEl.classList.add("show");
          }
        });
        everyitem.addEventListener("mouseleave", function (e) {
          let el_link = this.querySelector("a[data-bs-toggle]");

          if (el_link != null) {
            let nextEl = el_link.nextElementSibling;
            el_link.classList.remove("show");
            nextEl.classList.remove("show");
          }
        });
      });
  }
  // end if innerWidth
});
// DOMContentLoaded  end

$("#exampleModal").find('.modal-body').find('a').click(function (e) {
  const hrefVal = e.target.href;

  window.location.href = hrefVal;
})

const navbarSupportedContent = $("#navbarSupportedContent");
const depthMenu = navbarSupportedContent.parents('.kira_nav').find('.depth-menu')

navbarSupportedContent.find('.navbar-nav').hover(function () {
  depthMenu.addClass('open');
}); 

depthMenu.on("mouseleave", function () {
  depthMenu.removeClass('open');
});